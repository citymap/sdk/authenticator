# CityMap Authenticator

Allows generating valid signatures for requests made to the city-map API

# Documentation

Read the [wiki of this project](https://gitlab.com/citymap/authenticator/wikis/)